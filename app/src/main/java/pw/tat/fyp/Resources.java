package pw.tat.fyp;

import android.graphics.Typeface;

import org.andengine.audio.sound.Sound;
import org.andengine.audio.sound.SoundFactory;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import java.io.IOException;

/**
 * Created by indzz on 8/2/2017.
 */
public class Resources {
    private GameActivity game;
    private VertexBufferObjectManager vbom;

    public TiledTextureRegion mPlayerTextureRegion;

    public ITextureRegion bgSky;
    private ITextureRegion mParallaxLayerMid;
    private ITextureRegion mParallaxLayerFront;

    public ITextureRegion coinTexture;
    public ITextureRegion bombTexture;
    public ITextureRegion flagTexture;

    public ITextureRegion jumpTexture;
    public ITextureRegion squatBtnTexture;

    public ITextureRegion platformTexture;

    public Sound explosionSound;
    public Sound coinSound;
    public Sound winSound;

    public Font font;

    private static Resources ourInstance = new Resources();

    public static Resources getInstance() {
        return ourInstance;
    }

    public void setup(GameActivity game, VertexBufferObjectManager vbom) {
        getInstance().game = game;
        getInstance().vbom = vbom;

        setupTexture();
        setupFont();
        setupAudio();
    }

    private Resources() {

    }

    public void setupTexture() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        // Character sprite
        BitmapTextureAtlas characterTexture = new BitmapTextureAtlas(game.getTextureManager(), 264, 220, TextureOptions.BILINEAR);
        mPlayerTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(characterTexture, game, "character.png", 0, 0, 3, 2);
        characterTexture.load();

        // Parallax background
        BitmapTextureAtlas bgTexture = new BitmapTextureAtlas(game.getTextureManager(), 1080, 1920);
//        mParallaxLayerFront = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bgTexture, game, "background_ground.png", 0, 0);
        bgSky = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bgTexture, game, "background_sky.png", 0, 0);
//        mParallaxLayerMid = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bgTexture, game, "background_cloud.png", 0, 720);
        bgTexture.load();

        // Object
        BitmapTextureAtlas objectTextureAtlas = new BitmapTextureAtlas(game.getTextureManager(), 208, 200, TextureOptions.BILINEAR);

        coinTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectTextureAtlas, game, "coin.png", 0, 0);
        bombTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectTextureAtlas, game, "bomb.png", 0, 80);
        flagTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(objectTextureAtlas, game, "flag.png", 80, 0);
        objectTextureAtlas.load();

        // Platform
        BitmapTextureAtlas platformTextureAtlas = new BitmapTextureAtlas(game.getTextureManager(), 128, 45, TextureOptions.BILINEAR);
        platformTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(platformTextureAtlas, game, "platform.png", 0, 0);
        platformTextureAtlas.load();

        // Buttons
        BitmapTextureAtlas btnTextureAtlas = new BitmapTextureAtlas(game.getTextureManager(), 100, 50, TextureOptions.BILINEAR);
        jumpTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(btnTextureAtlas, game, "btn_jump.png", 0, 0);
        squatBtnTexture = BitmapTextureAtlasTextureRegionFactory.createFromAsset(btnTextureAtlas, game, "btn_down.png", 50, 0);
        btnTextureAtlas.load();
    }

    public void setupFont() {
        // Font
        final ITexture fontTexture = new BitmapTextureAtlas(game.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        font = new Font(game.getFontManager(), fontTexture, Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 32, true, new Color(25.0f/255.0f, 132.0f/255.0f, 122.0f/255.0f, 1));
        font.load();
    }

    public void setupAudio() {
        SoundFactory.setAssetBasePath("mfx/");
        try {
            explosionSound = SoundFactory.createSoundFromAsset(game.getSoundManager(), game, "explosion.ogg");
            coinSound = SoundFactory.createSoundFromAsset(game.getSoundManager(), game, "coin.ogg");
            winSound = SoundFactory.createSoundFromAsset(game.getSoundManager(), game, "win_long.ogg");
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}
