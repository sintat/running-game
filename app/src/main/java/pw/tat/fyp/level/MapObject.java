package pw.tat.fyp.level;

/**
 * Created by indzz on 9/2/2017.
 */

public class MapObject {
    public static final int TYPE_PLATFORM = 1;
    public static final int TYPE_COIN = 2;
    public static final int TYPE_BOMB = 3;
    public static final int TYPE_FLAG = 4;

    protected int x;
    protected int y;
    protected int type;

    public MapObject(int x, int y, int type) {
        this.x = x;
        this.y = y;
        this.type = type;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
