package pw.tat.fyp;

import android.os.Bundle;
import android.view.View;

import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.util.FPSLogger;
import org.andengine.ui.activity.SimpleBaseGameActivity;

/**
 * Scene design: http://www.freepik.com/free-vector/platform-game-in-a-nature-scene_947082.htm#term=game scene&page=1&position=7
 * Character: http://www.freepik.com/free-vector/nice-colorful-video-game-characters_948888.htm
 * Game: https://github.com/sizeofint/AndengineMarioEx
 */
public class GameActivity extends SimpleBaseGameActivity {
    private MainScene scene;
    private Resources resources;

    private BoundCamera mCamera;

    @Override
    protected void onCreate(final Bundle pSavedInstanceState) {
        hideSystemUI();
        super.onCreate(pSavedInstanceState);
    }

    @Override
    protected void onCreateResources() {
        resources = Resources.getInstance();
        resources.setup(this, getVertexBufferObjectManager());
    }

    @Override
    protected Scene onCreateScene() {
        resources = Resources.getInstance();
        resources.setup(this, getVertexBufferObjectManager());

        mEngine.registerUpdateHandler(new FPSLogger());
        scene = new MainScene(this);
        return scene;
    }

    @Override
    public EngineOptions onCreateEngineOptions() {
        mCamera = new BoundCamera(0, 0, GameConstants.CAMERA_WIDTH, GameConstants.CAMERA_HEIGHT);

        EngineOptions options = new EngineOptions(true, ScreenOrientation.PORTRAIT_FIXED, new RatioResolutionPolicy(GameConstants.CAMERA_WIDTH, GameConstants.CAMERA_HEIGHT), this.mCamera);
        options.getAudioOptions().setNeedsSound(true);

        return options;
    }

    public BoundCamera camera() {
        return mCamera;
    }

    // This snippet hides the system bars.
    private void hideSystemUI() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }
}
