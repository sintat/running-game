package pw.tat.fyp.level;

import java.util.List;

/**
 * Created by indzz on 9/2/2017.
 */

public abstract class LevelMap {
    public abstract List<MapObject> getItems();
    public abstract int boundWidth();
    public abstract int boundHeight();
}
