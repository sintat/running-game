package pw.tat.fyp;

import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

import org.andengine.engine.camera.BoundCamera;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.Constants;

import static org.andengine.extension.physics.box2d.util.constants.PhysicsConstants.PIXEL_TO_METER_RATIO_DEFAULT;

public abstract class Player extends AnimatedSprite
{
    private Body body;

    private boolean canRun = false;
    private boolean isDead = false;

    private int squatCount = 0;

    private boolean isRunning = false;
    private boolean isSquatting = false;
    private int faceDirection = FACE_DIRECTION_RIGHT;
    public final static int FACE_DIRECTION_LEFT = 1;
    public final static int FACE_DIRECTION_RIGHT = 0;

    private final static float SPEED_X = 3.5f;
    private final static float SPEED_Y = 8.0f;

    public Player(float pX, float pY, ITiledTextureRegion texture, VertexBufferObjectManager vbo, BoundCamera camera, PhysicsWorld physicsWorld)
    {
        super(pX, pY, texture.getWidth() * 0.5f, texture.getHeight() * 0.5f, texture, vbo);
        createPhysics(camera, physicsWorld);
        camera.setChaseEntity(this);
    }

    // ---------------------------------------------
    // CLASS LOGIC
    // ---------------------------------------------

    private void createPhysics(final BoundCamera camera, PhysicsWorld physicsWorld)
    {
        body = PhysicsFactory.createBoxBody(physicsWorld, this, BodyDef.BodyType.DynamicBody, PhysicsFactory.createFixtureDef(0, 0, 0));

        body.setUserData("player");
        body.setFixedRotation(true);


        physicsWorld.registerPhysicsConnector(new PhysicsConnector(this, body, true, false)
        {
            @Override
            public void onUpdate(float pSecondsElapsed)
            {
                super.onUpdate(pSecondsElapsed);
                camera.onUpdate(0.1f);

                if (isSquatting) {
                    squatCount--;
                    if (squatCount <= 0) {
                        standUp();
                    }
                }

                // maybe hit something
                if (isRunning && body.getLinearVelocity().x <= 0) {
                    autoRun();
                }

                // Check if out of bound
                if (getY() > camera.getBoundsYMax() && !isDead) {
                    onDie();
                    isDead = true;
                }
            }
        });
    }

    public void moveLeft() {
        faceDirection = FACE_DIRECTION_LEFT;
        isRunning = true;

        // Change velocity of X direction
        body.setLinearVelocity(new Vector2(-SPEED_X, body.getLinearVelocity().y));

        // Change sprite animation
        final long[] PLAYER_ANIMATE = new long[] { 100, 100, 100 };
        animate(PLAYER_ANIMATE, 3, 5, true);
    }

    public void moveRight() {
        faceDirection = FACE_DIRECTION_RIGHT;
        isRunning = true;

        // Change velocity of X direction
        body.setLinearVelocity(new Vector2(SPEED_X, body.getLinearVelocity().y));

        // Change sprite animation
        final long[] PLAYER_ANIMATE = new long[] { 100, 100, 100 };
        animate(PLAYER_ANIMATE, 0, 2, true);
    }

    public void autoRun() {
        faceDirection = FACE_DIRECTION_RIGHT;
        isRunning = true;

        // Change velocity of X direction
        body.setLinearVelocity(new Vector2(SPEED_X, body.getLinearVelocity().y));

        // Change sprite animation
        final long[] PLAYER_ANIMATE = new long[] { 100, 100, 100 };
        if (isSquatting) {
            animate(PLAYER_ANIMATE, 3, 5, true);
        } else {
            animate(PLAYER_ANIMATE, 0, 2, true);
        }
    }

    public void jump()
    {
        body.setLinearVelocity(new Vector2(body.getLinearVelocity().x, -SPEED_Y));
    }

    public void squat() {
        Fixture fixture = body.getFixtureList().get(0);
        if (fixture != null) {
            float heightDelta = 30.0f;

            float hw = getWidth() * 0.5f;
            float hh = getHeight() * 0.5f;

            PolygonShape shape = (PolygonShape) fixture.getShape();
            shape.setAsBox(hw / PIXEL_TO_METER_RATIO_DEFAULT,
                    (hh - (heightDelta * 0.5f)) / PIXEL_TO_METER_RATIO_DEFAULT,
                    new Vector2(0.0f, (heightDelta * 0.5f) / PIXEL_TO_METER_RATIO_DEFAULT),
                    0.0f);
        }

        squatCount = 100;
        isSquatting = true;
        autoRun();
    }

    public void standUp() {
        if (!isSquatting) return;

        Fixture fixture = body.getFixtureList().get(0);
        if (fixture != null) {
            float hw = getWidth() * 0.5f;
            float hh = getHeight() * 0.5f;

            PolygonShape shape = (PolygonShape) fixture.getShape();
            shape.setAsBox(hw / PIXEL_TO_METER_RATIO_DEFAULT,
                    hh / PIXEL_TO_METER_RATIO_DEFAULT,
                    new Vector2(0.0f, 0.0f),
                    0.0f);
        }

        isSquatting = false;
        autoRun();
    }

    public abstract void onDie();

    public void onGameFinished() {
        isRunning = false;
    }
}