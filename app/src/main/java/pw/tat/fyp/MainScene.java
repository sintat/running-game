package pw.tat.fyp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.HorizontalAlign;

import java.util.List;

import pw.tat.fyp.level.Level1;
import pw.tat.fyp.level.LevelMap;
import pw.tat.fyp.level.MapObject;

//http://www.matim-dev.com/full-game-tutorial---part-11.html
public class MainScene extends Scene {
    private GameActivity game;
    private Resources resources;
    private Player player;

    private HUD gameHUD;

    private Text scoreText;
    private int score = 0;

    private Text hpText;
    private int hp = 100;

    private PhysicsWorld physicsWorld;
    private boolean stopManagedUpdate = false;

    private VertexBufferObjectManager vertexBufferObjectManager;

    public MainScene(GameActivity game) {
        super();

        this.game = game;
        resources = Resources.getInstance();

        createScene();
    }

    public void createScene() {
        vertexBufferObjectManager = game.getVertexBufferObjectManager();

        setupBackground();

        setupPhysics(); // physics must be setup BEFORE player

        setupPlayer();
        createHUD();    // On-screen control
        loadLevel(new Level1());

        player.autoRun();
    }

    private void createHUD() {
        gameHUD = new HUD();

        gameHUD.setTouchAreaBindingOnActionDownEnabled(true);
        gameHUD.setTouchAreaBindingOnActionMoveEnabled(true);

        // Score
        scoreText = new Text(20, 10, resources.font, "Score: 0123456789", new TextOptions(HorizontalAlign.LEFT), vertexBufferObjectManager);
        scoreText.setText("Score: "+score);
        gameHUD.attachChild(scoreText);

        // HP
        hpText = new Text(GameConstants.CAMERA_WIDTH - 140, 10, resources.font, "HP: 0123456789", new TextOptions(HorizontalAlign.RIGHT), vertexBufferObjectManager);
        hpText.setText("HP: " + hp);
        hpText.setWidth(120);
        gameHUD.attachChild(hpText);

        // Jump button
        final Sprite jumpButton = new Sprite(40, GameConstants.CAMERA_HEIGHT - 170, resources.jumpTexture, vertexBufferObjectManager) {
            @Override
            public boolean onAreaTouched(final TouchEvent pSceneTouchEvent,
                                         final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionUp()) {
                    player.jump();
                }
                return true;
            };
        };

        // Move down button
        final Sprite downButton = new Sprite(40, GameConstants.CAMERA_HEIGHT - 100, resources.squatBtnTexture, vertexBufferObjectManager) {
            @Override
            public boolean onAreaTouched(final TouchEvent pSceneTouchEvent,
                                         final float pTouchAreaLocalX, final float pTouchAreaLocalY) {
                if (pSceneTouchEvent.isActionUp()) {
                    player.squat();
                }
                return true;
            };
        };
        gameHUD.attachChild(jumpButton);
        gameHUD.attachChild(downButton);

        gameHUD.registerTouchArea(jumpButton);
        gameHUD.registerTouchArea(downButton);

        game.camera().setHUD(gameHUD);
    }

    @Override
    public void onManagedUpdate(final float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);
        if (this.stopManagedUpdate)
            return;
    }

    private void loadLevel(LevelMap lv)
    {
        List<MapObject> objects = lv.getItems();

        // Create all objects in the current levels
        for (MapObject object : objects) {
            Sprite levelObject;

            if (object.getType() == MapObject.TYPE_PLATFORM) {
                levelObject = new Sprite(object.getX(), object.getY(), resources.platformTexture, vertexBufferObjectManager);
                PhysicsFactory.createBoxBody(physicsWorld, levelObject, BodyDef.BodyType.StaticBody, GameConstants.FIXTURE_DEF).setUserData("platform1");
            } else if (object.getType() == MapObject.TYPE_COIN) {
                levelObject = new Sprite(object.getX(), object.getY(), resources.coinTexture.getWidth()*0.5f, resources.coinTexture.getHeight()*0.5f, resources.coinTexture, vertexBufferObjectManager) {
                    @Override
                    protected void onManagedUpdate(float pSecondsElapsed)
                    {
                        super.onManagedUpdate(pSecondsElapsed);

                        if (player.collidesWith(this))
                        {
                            // play get coin sound
                            resources.coinSound.play();

                            // add score
                            score += 25;
                            scoreText.setText(String.format("Score: %d", score));

                            this.setVisible(false);
                            this.setIgnoreUpdate(true);
                        }
                    }
                };
            } else if (object.getType() == MapObject.TYPE_BOMB) {
                levelObject = new Sprite(object.getX(), object.getY(), resources.bombTexture.getWidth()*0.5f, resources.bombTexture.getHeight()*0.5f, resources.bombTexture, vertexBufferObjectManager) {
                    @Override
                    protected void onManagedUpdate(float pSecondsElapsed)
                    {
                        super.onManagedUpdate(pSecondsElapsed);

                        if (player.collidesWith(this))
                        {
                            // play explosion sound
                            resources.explosionSound.play();

                            // deduct HP value
                            hp -= 25;
                            hpText.setText(String.format("HP: %d", hp));

                            if (hp <= 0) {
                                dieAlert();
                            }

                            this.setVisible(false);
                            this.setIgnoreUpdate(true);
                        }
                    }
                };
            } else if (object.getType() == MapObject.TYPE_FLAG) {
                levelObject = new Sprite(object.getX(), object.getY(), resources.flagTexture.getWidth()*0.5f, resources.flagTexture.getHeight()*0.5f, resources.flagTexture, vertexBufferObjectManager) {
                    @Override
                    protected void onManagedUpdate(float pSecondsElapsed)
                    {
                        super.onManagedUpdate(pSecondsElapsed);

                        if (player.collidesWith(this))
                        {
                            winAlert();
                            this.setIgnoreUpdate(true);
                        }
                    }
                };
            } else {
                throw new IllegalArgumentException("Unknown type of map object");
            }

            levelObject.setCullingEnabled(true);
            attachChild(levelObject);
        }

        // Set camera bound
        game.camera().setBounds(0, 0, lv.boundWidth(), lv.boundHeight());
        game.camera().setBoundsEnabled(true);
    }

    public void disposeScene() {
        this.stopManagedUpdate = true;
        game.camera().setHUD(null);
        game.camera().setChaseEntity(null);
        game.camera().setCenter(GameConstants.CAMERA_WIDTH / 2, GameConstants.CAMERA_HEIGHT / 2);
        this.detachSelf();
        this.dispose();
    }

    protected void setupBackground() {
        // Background
        SpriteBackground bg = new SpriteBackground(174.0f/255.0f, 244.0f/255.0f, 249.0f/255.0f, new Sprite(0, 0, GameConstants.CAMERA_WIDTH, GameConstants.CAMERA_HEIGHT, resources.bgSky, vertexBufferObjectManager));
        setBackground(bg);
    }

    protected void setupPhysics() {
        physicsWorld = new FixedStepPhysicsWorld(60, new Vector2(0f, GameConstants.GRAVITY), false);
        registerUpdateHandler(physicsWorld);
    }

    protected void setupPlayer() {
        // Player
		/* Calculate the coordinates for the face, so its centered on the camera. */
        final float playerX = 20;
        final float playerY = GameConstants.CAMERA_HEIGHT - resources.mPlayerTextureRegion.getHeight() - 100;

		/* Create player and add it to the scene. */
        player = new Player(playerX, playerY, Resources.getInstance().mPlayerTextureRegion, vertexBufferObjectManager, game.camera(), physicsWorld) {
            @Override
            public void onDie() {
                dieAlert();
            }
        };
        attachChild(player);
    }

    protected void winAlert() {
        resources.winSound.play();
        game.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(game);
                builder.setTitle("Finished");
                builder.setMessage("You win! Yeah!!");
                builder.setCancelable(false);
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        game.finish();
                    }
                });
                builder.show();
            }
        });
    }

    protected void dieAlert() {
        game.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(game);
                builder.setTitle("Game Over!");
                builder.setMessage("Oops! You lose!!");
                builder.setCancelable(false);
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        game.finish();
                    }
                });
                builder.show();
            }
        });
    }
}
