package pw.tat.fyp;

import com.badlogic.gdx.physics.box2d.FixtureDef;

import org.andengine.extension.physics.box2d.PhysicsFactory;

/**
 * Created by indzz on 8/2/2017.
 */

public class GameConstants {
    public static final int CAMERA_WIDTH = 405;
    public static final int CAMERA_HEIGHT = 720;
    public static final float GRAVITY = 20.0f;

    /* The categories. */
    public static final short CATEGORYBIT_WALL = 1;
    public static final short CATEGORYBIT_PLAYER = 2;
    public static final short CATEGORYBIT_BULLET = 4;
    public static final short CATEGORYBIT_ENEMY = 8;
    public static final short CATEGORYBIT_ENEMYWALL = 16;
    public static final short CATEGORYBIT_PLAYERBODY = 32;
    public static final short CATEGORYBIT_PLAYERBULLET = 64;
    public static final short CATEGORYBIT_ENEMYBODY = 128;

    /* And what should collide with what. */
    public static final short MASKBITS_WALL = -1;
    public static final short MASKBITS_PLAYER = CATEGORYBIT_WALL;
    public static final short MASKBITS_BULLET = CATEGORYBIT_WALL + CATEGORYBIT_PLAYERBODY;
    public static final short MASKBITS_ENEMY = CATEGORYBIT_WALL + CATEGORYBIT_ENEMYWALL;
    public static final short MASKBITS_ENEMYWALL = CATEGORYBIT_ENEMY + CATEGORYBIT_WALL + CATEGORYBIT_ENEMYBODY;
    public static final short MASKBITS_PLAYERBODY = CATEGORYBIT_BULLET;
    public static final short MASKBITS_PLAYERBULLET = CATEGORYBIT_WALL + CATEGORYBIT_ENEMYBODY;
    public static final short MASKBITS_ENEMYBODY = CATEGORYBIT_WALL + CATEGORYBIT_ENEMYWALL + CATEGORYBIT_PLAYERBULLET;

    public static final FixtureDef FIXTURE_DEF = PhysicsFactory.createFixtureDef(0, 0, 0);
}
