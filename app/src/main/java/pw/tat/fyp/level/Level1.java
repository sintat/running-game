package pw.tat.fyp.level;

import java.util.ArrayList;
import java.util.List;

public class Level1 extends LevelMap {
    @Override
    public List<MapObject> getItems() {
        ArrayList<MapObject> items = new ArrayList<>();

        items.add(new MapObject(0, 640, MapObject.TYPE_PLATFORM));
        items.add(new MapObject(128, 640, MapObject.TYPE_PLATFORM));
        items.add(new MapObject(256, 640, MapObject.TYPE_PLATFORM));
        items.add(new MapObject(384, 640, MapObject.TYPE_PLATFORM));
        items.add(new MapObject(512, 640, MapObject.TYPE_PLATFORM));

        items.add(new MapObject(190, 490, MapObject.TYPE_COIN));
        items.add(new MapObject(250, 490, MapObject.TYPE_COIN));
        items.add(new MapObject(180, 550, MapObject.TYPE_PLATFORM));

        items.add(new MapObject(370, 440, MapObject.TYPE_COIN));
        items.add(new MapObject(440, 440, MapObject.TYPE_BOMB));
        items.add(new MapObject(360, 480, MapObject.TYPE_PLATFORM));

        items.add(new MapObject(550, 360, MapObject.TYPE_COIN));
        items.add(new MapObject(640, 360, MapObject.TYPE_BOMB));
        items.add(new MapObject(550, 400, MapObject.TYPE_PLATFORM));

        items.add(new MapObject(730, 260, MapObject.TYPE_COIN));
        items.add(new MapObject(780, 260, MapObject.TYPE_COIN));
        items.add(new MapObject(730, 300, MapObject.TYPE_PLATFORM));

        items.add(new MapObject(830, 310, MapObject.TYPE_COIN));
        items.add(new MapObject(830, 350, MapObject.TYPE_PLATFORM));

        items.add(new MapObject(750, 410, MapObject.TYPE_BOMB));
        items.add(new MapObject(740, 450, MapObject.TYPE_PLATFORM));
        items.add(new MapObject(868, 410, MapObject.TYPE_COIN));
        items.add(new MapObject(868, 450, MapObject.TYPE_PLATFORM));
        items.add(new MapObject(996, 450, MapObject.TYPE_PLATFORM));

        items.add(new MapObject(1050, 355, MapObject.TYPE_PLATFORM));
        items.add(new MapObject(1050, 315, MapObject.TYPE_COIN));
        items.add(new MapObject(1130, 315, MapObject.TYPE_BOMB));

        items.add(new MapObject(1572, 450, MapObject.TYPE_PLATFORM));
        items.add(new MapObject(1610, 355, MapObject.TYPE_FLAG));

        return items;
    }

    @Override
    public int boundWidth() {
        return 1700;
    }

    @Override
    public int boundHeight() {
        return 720;
    }
}
